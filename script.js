const images = document.querySelectorAll(".image-to-show");
const timerDisplay = document.getElementById("timer");
const startButton = document.getElementById("startButton");
const resumeButton = document.getElementById("resumeButton");
let currentIndex = 0;
let intervalId;
let countdown = 3;
let remainingTime = 3;
let slideshowRunning = true;

function showImage(index) {
    images.forEach(image => {
        image.style.opacity = 0;
        image.style.display = "none";
    });

    images[index].style.display = "block";
    setTimeout(() => {
        images[index].style.opacity = 1;
    }, 10);
}

function startSlideshow() {
    showImage(currentIndex);

    intervalId = setInterval(() => {
        if (slideshowRunning) {
            countdown = 3;
            updateTimerDisplay();
            currentIndex = (currentIndex + 1) % images.length;
            showImage(currentIndex);
        }
    }, 3000);
}

function updateTimerDisplay() {
    timerDisplay.textContent = `Таймер: ${countdown} сек`;
}

startButton.addEventListener("click", () => {
    clearInterval(intervalId);
    slideshowRunning = false;
    remainingTime = countdown;
    resumeButton.disabled = false;
    startButton.disabled = true;
});

resumeButton.addEventListener("click", () => {
    clearInterval(intervalId);
    slideshowRunning = true;
    countdown = remainingTime;
    startSlideshow();
    resumeButton.disabled = true;
    startButton.disabled = false;
});

setInterval(() => {
    if (slideshowRunning && countdown > 0) {
        countdown--;
        updateTimerDisplay();
    }
}, 1000);
startSlideshow();